import { Component, Input } from '@angular/core';
import { IMovieViewModel } from '../movie.interfaces';
import { IGenre } from '../../genre/genre.interface';

@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.less']
})
export class MovieCardComponent {
  @Input() movie: IMovieViewModel;

  getGenresString(genres: IGenre[] = []) {
    return genres.map(g => g.name).join(', ');
  }
}

import { IGenre } from '../genre/genre.interface';

interface IMovieBaseModel {
  vote_count: number;
  id: number;
  video: boolean;
  vote_average: number;
  title: string;
  popularity: number;
  original_language: string;
  original_title: string;
  backdrop_path: string;
  adult: boolean;
  overview: string;
  release_date: Date;
}

interface IMovieListBaseModel {
  page: number;
  total_results: number;
  total_pages: number;
}

export interface IMovieApiModel extends IMovieBaseModel {
  genre_ids: Array<number>;
  poster_path: string;
}

export interface IMovieViewModel extends IMovieBaseModel {
  genres: Array<IGenre>;
  poster_url: string;
}

export interface IMovieListApiModel extends IMovieListBaseModel {
  results: Array<IMovieApiModel>;
}

export interface IMovieListViewModel extends IMovieListBaseModel {
  results: Array<IMovieViewModel>;
}

export interface IMovieDetailsModel {
  adult: boolean;
  backdrop_path: string;
  belongs_to_collection: string;
  budget: number;
  genres: IGenre[];
  homepage: string;
  id: number;
  imdb_id: string;
  original_language: string;
  original_title: string;
  overview: string;
  popularity: number;
  poster_path: string;
  production_companies: IProductionCompany[];
  release_date: string;
  revenue: number;
  runtime: number;
  status: string;
  tagline: string;
  title: string;
  video: boolean;
  vote_average: number;
  vote_count: number;
}

export interface IProductionCompany {
  id: number;
  logo_path: string;
  name: string;
  origin_country: string;
}

export interface IMovieDetailsBaseImageModel {
  aspect_ratio: number;
  file_path: string;
  height: number;
  iso_639_1: string;
  vote_average: number;
  vote_count: number;
  width: number;
}

export interface IMovieDetailsImagesModel {
  id: number;
  backdrops: IMovieDetailsBaseImageModel[];
  posters: IMovieDetailsBaseImageModel[];
}

export interface IMovieDetailsBaseVideoModel {
  id: string;
  iso_639_1: string;
  iso_3166_1: string;
  key: string;
  name: string;
  site: string;
  size: number;
  type: string;
}

export interface IMovieDetailsVideosModel {
  id: number;
  results: IMovieDetailsBaseVideoModel[];
}

export interface IMovieDetailsPersonBaseModel {
  gender: number;
  id: number;
  name: string;
  profile_path: string;
  credit_id: string;
}

export interface IMovieDetailsActorModel extends IMovieDetailsPersonBaseModel {
  cast_id: number;
  character: string;
  order: number;
}

export interface IMovieDetailsStaffModel extends IMovieDetailsPersonBaseModel {
  department: string;
  job: string;
}

export interface IMovieDetailsCreditsModel {
  id: number;
  cast: IMovieDetailsActorModel[];
  crew: IMovieDetailsStaffModel[];
}

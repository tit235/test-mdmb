import { Component, OnDestroy } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Subscription } from 'rxjs';

import { TmdbService } from '../../api/tmdb.service';
import { IMovieListApiModel, IMovieViewModel } from '../movie.interfaces';
import { GenreService } from '../../genre/genre.service';
import { IGenre } from '../../genre/genre.interface';
import { SettingsService } from '../../settings/settings.service';

@Component({
  selector: 'app-movie-list',
  templateUrl: './movie-list.component.html'
})
export class MovieListComponent implements OnDestroy {
  private subscriptions: Subscription[] = [];
  movies: IMovieViewModel[];
  genres: IGenre[];

  page: number;
  totalResults: number;
  pageSize = 20;

  sorts = [
    'popularity.asc',
    'popularity.desc',
    'release_date.asc',
    'release_date.desc',
    'vote_average.asc',
    'vote_average.desc'
  ];

  defaultSort = 'vote_average.desc';
  activeSort = this.defaultSort;

  currentQueryParams: any = {};

  constructor(private tmdbService: TmdbService,
              private genreService: GenreService,
              private settingsService: SettingsService,
              private route: ActivatedRoute,
              private router: Router) {
    this.subscriptions.push(this.genreService.genres$.subscribe(res => this.genres = res));

    this.subscriptions.push(this.settingsService.language$.subscribe(lang => {
      this.fetchMovies(this.currentQueryParams);
      
      this.activeSort = this.currentQueryParams.sort_by || this.defaultSort;
    }));

    this.subscriptions.push(this.route.queryParamMap.subscribe(map => {
      this.currentQueryParams = map['params'];
      this.fetchMovies(map['params']);
      
      this.activeSort = map['params'].sort_by || this.defaultSort;
    }));
  }

  fetchMovies(options?) {
    this.tmdbService.discover.getMovies(options)
      .then((res: IMovieListApiModel) => {
        if (!res) {
          return;
        }

        this.movies = res.results.map(item => {
          const genreIds = item.genre_ids;
          const vm: IMovieViewModel = {
            vote_count: item.vote_count,
            id: item.id,
            video: item.video,
            vote_average: item.vote_average,
            title: item.title,
            popularity: item.popularity,
            poster_url: `${this.tmdbService.common.images_uri}w185_and_h278_bestv2/${item.poster_path}`,
            original_language: item.original_language,
            original_title: item.original_title,
            backdrop_path: item.backdrop_path,
            adult: item.adult,
            overview: item.overview,
            release_date: item.release_date,
            genres: []
          };

          genreIds.forEach(id => {
            vm.genres.push(this.genres.find(genre => genre.id === id));
          });

          return vm;
        });

        this.page = res.page;
        this.totalResults = res.total_results;
      });
  }

  onPageChange(newPage: number) {
    this.router.navigate([], { queryParams: { page: newPage }, queryParamsHandling: 'merge' });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}

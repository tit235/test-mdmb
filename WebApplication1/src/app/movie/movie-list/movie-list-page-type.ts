export enum MovieListPageType {
  NowPlaying = 1,
  Discover = 2
}

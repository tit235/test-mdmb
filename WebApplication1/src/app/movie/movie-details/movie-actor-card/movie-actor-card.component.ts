import { Component, Input } from '@angular/core';
import { IMovieDetailsActorModel } from '../../movie.interfaces';

@Component({
  selector: 'app-movie-actor-card',
  templateUrl: './movie-actor-card.component.html',
  styleUrls: ['./movie-actor-card.component.less']
})
export class MovieActorCardComponent {
  @Input() actor: IMovieDetailsActorModel;
}

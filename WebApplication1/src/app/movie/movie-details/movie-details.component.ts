import { Component, OnDestroy } from '@angular/core';
import {
  IMovieDetailsModel,
  IMovieDetailsImagesModel,
  IMovieDetailsCreditsModel,
  IMovieDetailsVideosModel,
  IMovieListApiModel,
  IMovieDetailsActorModel,
  IMovieDetailsBaseImageModel
} from '../movie.interfaces';
import { TmdbService } from '../../api/tmdb.service';
import { SettingsService } from '../../settings/settings.service';
import { ActivatedRoute } from '@angular/router';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-details-list',
  templateUrl: './movie-details.component.html'
})
export class MovieDetailsComponent implements OnDestroy {
  private subscriptions: Subscription[] = [];
  private movieId: number;

  movie: IMovieDetailsModel;
  images: IMovieDetailsImagesModel;
  displayImages: IMovieDetailsBaseImageModel[];
  credits: IMovieDetailsCreditsModel;
  displayActors: IMovieDetailsActorModel[];
  videos: IMovieDetailsVideosModel;
  similarMovies: IMovieListApiModel;

  constructor(private tmdbService: TmdbService,
              private settingsService: SettingsService,
              private route: ActivatedRoute) {

    this.route.params.subscribe(params => {
      this.movieId = params['id'];
      this.fetchData();
    });

    this.subscriptions.push(this.settingsService.language$.subscribe(lang => {
      this.fetchData();
    }));
  }

  fetchData() {
    this.fetchMovie();
    this.fetchImages();
    this.fetchCredits();
    this.fetchVideos();
    this.fetchSimilarMovies();
  }

  fetchMovie() {
    this.tmdbService.movies.getById({ id: this.movieId })
      .then((res: IMovieDetailsModel) => {
        res.poster_path = `${this.tmdbService.common.images_uri}w600_and_h900_bestv2/${res.poster_path}`;
        this.movie = res;
      });
  }

  fetchImages() {
    this.tmdbService.movies.getImages({ id: this.movieId })
      .then((res: IMovieDetailsImagesModel) => {
        this.images = res;

        this.images.posters = this.images.posters.map(c => {
          c.file_path = `${this.tmdbService.common.images_uri}w200_and_h300_bestv2/${c.file_path}`;
          return c;
        });

        this.displayImages = this.images.posters.slice(0, 5);
      });
  }

  fetchCredits() {
    this.tmdbService.movies.getCredits({ id: this.movieId })
      .then((res: IMovieDetailsCreditsModel) => {
        this.credits = res;
        this.credits.cast = this.credits.cast.map(c => {
          c.profile_path = `${this.tmdbService.common.images_uri}w138_and_h175_face/${c.profile_path}`;
          return c;
        });

        this.displayActors = this.credits.cast.slice(0, 5);
      });
  }

  fetchVideos() {
    this.tmdbService.movies.getVideos({ id: this.movieId })
      .then((res: IMovieDetailsVideosModel) => this.videos = res);
  }

  fetchSimilarMovies() {
    this.tmdbService.movies.getSimilarMovies({ id: this.movieId })
      .then((res: IMovieListApiModel) => this.similarMovies = res);
  }

  showMoreActors() {
    this.displayActors = this.credits.cast;
  }

  showMoreImages() {
    this.displayImages = this.images.posters;
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}

import { Injectable, OnDestroy } from '@angular/core';
import { Http } from '@angular/http';
import { Subscription } from 'rxjs';

import { SettingsService } from '../settings/settings.service';

@Injectable()
export class TmdbService implements OnDestroy {
  private subscriptions: Subscription[] = [];

  constructor(private http: Http, private settingsService: SettingsService) {
    this.subscriptions.push(this.settingsService.language$.subscribe(lang => this.common.language = lang));
  }

  common = {
    api_key: '9ebabe10e45dfe1bf92c00f3b29bb8a7',
    base_uri: 'http://api.themoviedb.org/3/',
    images_uri: 'http://image.tmdb.org/t/p/',
    timeout: 5000,
    language: 'en-US',
    generateQuery: (options?) => {
      let myOptions, query, option;

      myOptions = options || {};
      query = '?api_key=' + this.common.api_key + '&language=' + this.common.language;

      if (Object.keys(myOptions).length > 0) {
        for (option in myOptions) {
          if (myOptions.hasOwnProperty(option) && option !== 'id' && option !== 'body') {
            query = query + '&' + option + '=' + myOptions[option];
          }
        }
      }
      return query;
    },
    getImage: (options) => {
      return this.common.images_uri + options.size + '/' + options.file;
    },
    client: (options) => {
      let action;
      if (options.method === 'POST') {
        action = this.http.post(this.common.base_uri + options.url, JSON.stringify(options.body));
      } else {
        action = this.http.get(this.common.base_uri + options.url);
      }

      return action.toPromise().then(res => res.json());
    }
  };

  configurations = {
    getConfiguration: () => {
      return this.common.client({
        url: 'configuration' + this.common.generateQuery()
      });
    },
    getCountries: () => {
      return this.common.client({
        url: 'configuration/countries' + this.common.generateQuery()
      });
    },
    getJobs: () => {
      return this.common.client({
        url: 'configuration/jobs' + this.common.generateQuery()
      });
    },
    getLanguages: () => {
      return this.common.client({
        url: 'configuration/languages' + this.common.generateQuery()
      });
    },
    getPrimaryTranslations: () => {
      return this.common.client({
        url: 'configuration/primary_translations' + this.common.generateQuery()
      });
    },
    getTimezones: () => {
      return this.common.client({
        url: 'configuration/timezones' + this.common.generateQuery()
      });
    }
  };

  account = {
    getInformation: (options) => {
      return this.common.client({
        url: 'account' + this.common.generateQuery(options)
      });
    },
    getLists: (options) => {
      return this.common.client({
        url: 'account/' + options.id + '/lists' + this.common.generateQuery(options)
      });
    },
    getFavoritesMovies: (options) => {
      return this.common.client({
        url: 'account/' + options.id + '/favorite/movies' + this.common.generateQuery(options)
      });
    },
    getFavoritesTvShows: (options) => {
      return this.common.client({
        url: 'account/' + options.id + '/favorite/tv?' + this.common.generateQuery(options)
      });
    },
    addFavorite: (options) => {
      const body = {
        'media_type': options.media_type,
        'media_id': options.media_id,
        'favorite': options.favorite
      };  
  
      return this.common.client({
        url: 'account/' + options.id + '/favorite' + this.common.generateQuery(options),
        status: 201,
        method: 'POST',
        body: body
      });
    },
    getRatedMovies: (options) => {
      return this.common.client({
        url: 'account/' + options.id + '/rated/movies' + this.common.generateQuery(options)
      });
    },
    getRatedTvShows: (options) => {
      return this.common.client({
        url: 'account/' + options.id + '/rated/tv' + this.common.generateQuery(options)
      });
    },
    getRatedTvEpisodes: (options) => {
      return this.common.client({
        url: 'account/' + options.id + 'rated/tv/episodes' + this.common.generateQuery(options)
      });
    },
    getMovieWatchlist: (options) => {
      return this.common.client({
        url: 'account/' + options.id + '/watchlist/movies' + this.common.generateQuery(options)
      });
    },
    getTvShowsWatchlist: (options) => {
      return this.common.client({
        url: 'account/' + options.id + '/watchlist/tv' + this.common.generateQuery(options)
      });
    },
    addToWatchlist: (options) => {
      const body = {
        'media_type': options.media_type,
        'media_id': options.media_id,
        'watchlist': options.watchlist
      };
  
      return this.common.client({
        url: 'account/' + options.id + '/watchlist' + this.common.generateQuery(options),
        method: 'POST',
        status: 201,
        body: body
      });
    }
  };

  authentication = {
    generateToken: () => {
      return this.common.client({
        url: 'authentication/token/new' + this.common.generateQuery()
      });
    },
    askPermissions: (options) => {
      window.open('https://www.themoviedb.org/authenticate/' + options.token + '?redirect_to=' + options.redirect_to);
    },
    validateUser: (options) => {
      return this.common.client({
        url: 'authentication/token/validate_with_login' + this.common.generateQuery(options)
      });
    },
    generateSession: (options) => {
      return this.common.client({
        url: 'authentication/session/new' + this.common.generateQuery(options)
      });
    },
    generateGuestSession: () => {
      return this.common.client({
        url: 'authentication/guest_session/new' + this.common.generateQuery()
      });
    }
  };

  certifications = {
    getMovieList: () => {
      return this.common.client({
        url: 'certification/movie/list' + this.common.generateQuery()
      });
    },
    getTvList: () => {
      return this.common.client({
        url: 'certification/tv/list' + this.common.generateQuery()
      });
    }
  };

  changes = {
    getMovieChanges: (options) => {
      return this.common.client({
        url: 'movie/changes' + this.common.generateQuery(options)
      });
    },
    getPersonChanges: (options) => {
      return this.common.client({
        url: 'person/changes' + this.common.generateQuery(options)
      });
    },
    getTvChanges: (options) => {
      return this.common.client({
        url: 'tv/changes' + this.common.generateQuery(options)
      });
    }
  };

  collections = {
    getDetails: (options) => {
      return this.common.client({
        url: 'collection/' + options.id + this.common.generateQuery(options)
      });
    },
    getImages: (options) => {
      return this.common.client({
        url: 'collection/' + options.id + '/images' + this.common.generateQuery(options)
      });
    },
    getTranslations: (options) => {
      return this.common.client({
        url: 'collection/' + options.id + '/translations' + this.common.generateQuery(options)
      });
    }
  };

  companies = {
    getDetails: (options) => {
      return this.common.client({
        url: 'company/' + options.id + this.common.generateQuery(options)
      });
    },
    getAlternativeNames: (options) => {
      return this.common.client({
        url: 'company/' + options.id + '/alternative_names' + this.common.generateQuery(options)
      });
    }
  };

  credits = {
    getDetails: (options) => {
      return this.common.client({
        url: 'credit/' + options.id + this.common.generateQuery(options)
      });
    }
  };

  discover = {
    getMovies: (options?) => {
      return this.common.client({
        url: 'discover/movie' + this.common.generateQuery(options)
      });
    },
    getTvShows: (options) => {
      return this.common.client({
        url: 'discover/tv' + this.common.generateQuery(options)
      });
    }
  };

  find = {
    getById: function(options) {
      return this.common.client({
        url: 'find/' + options.id + this.common.generateQuery(options)
      });
    }
  };

  genres = {
    getMovieList: (options?) => {
      return this.common.client({
        url: 'genre/movie/list' + this.common.generateQuery(options)
      });
    },
    getMovies: (options) => {
      return this.common.client({
        url: 'genre/' + options.id + '/movies' + this.common.generateQuery(options)
      });
    },
    getTvList: (options) => {
      return this.common.client({
        url: 'genre/tv/list' + this.common.generateQuery(options)
      });
    }
  };

  guestSession = {
    getRatedMovies: (options) => {
      return this.common.client({
        url: 'guest_session/' + options.id + '/rated/movies' + this.common.generateQuery()
      });
    },
    getRatedTvShows: (options) => {
      return this.common.client({
        url: 'guest_session/' + options.id + '/rated/tv' + this.common.generateQuery()
      });
    },
    getRatedTvEpisodes: (options) => {
      return this.common.client({
        url: 'guest_session/' + options.id + '/rated/tv/episodes' + this.common.generateQuery()
      });
    }
  };

  keywords = {
    getById: (options) => {
      return this.common.client({
        url: 'keyword/' + options.id + this.common.generateQuery(options)
      });
    },
    getMovies: (options) => {
      return this.common.client({
        url: 'keyword/' + options.id + '/movies' + this.common.generateQuery(options)
      });
    }
  };

  lists = {
    getById: (options) => {
      return this.common.client({
        url: 'list/' + options.id + this.common.generateQuery(options)
      });
    },
    getStatusById: (options) => {
      return this.common.client({
        url: 'list/' + options.id + '/item_status' + this.common.generateQuery(options)
      });
    },
    addList: (options) => {
      const body = {
        'name': options.name,
        'description': options.description
      };
  
      delete options.name;
      delete options.description;
  
      if (options.hasOwnProperty('language')) {
        body['language'] = options.language;
  
        delete options.language;
      }
  
      return this.common.client({
          method: 'POST',
          status: 201,
          url: 'list' + this.common.generateQuery(options),
        body: body
      });
    },
    addItem: (options) => {
      const body = {
        'media_id': options.media_id
      };
  
      return this.common.client({
          method: 'POST',
          status: 201,
          url: 'list/' + options.id + '/add_item' + this.common.generateQuery(options),
        body: body
      });
    },
    removeItem: (options) => {
      const body = {
        'media_id': options.media_id
      };
  
      return this.common.client({
          method: 'POST',
          status: 201,
          url: 'list/' + options.id + '/remove_item' + this.common.generateQuery(options),
        body: body
      });
    },
    removeList: (options) => {
      return this.common.client({
          method: 'DELETE',
          status: 204,
        url: 'list/' + options.id + this.common.generateQuery(options)
      });
    },
    clearList: (options) => {
      return this.common.client({
          method: 'POST',
          status: 204,
          body: {},
        url: 'list/' + options.id + '/clear' + this.common.generateQuery(options)
      });
    }
  };

  movies = {
    getById: (options) => {
      return this.common.client({
        url: 'movie/' + options.id + this.common.generateQuery(options)
      });
    },
    getAccountStates: (options) => {
      return this.common.client({
        url: 'movie/' + options.id + '/account_states' + this.common.generateQuery(options)
      });
    },
    getAccountStatesGuest: (options) => {
      return this.common.client({
        url: 'movie/' + options.id + '/account_states' + this.common.generateQuery(options)
      });
    },
    getAlternativeTitles: (options) => {
      return this.common.client({
        url: 'movie/' + options.id + '/alternative_titles' + this.common.generateQuery(options)
      });
    },
    getChanges: (options) => {
      return this.common.client({
        url: 'movie/' + options.id + '/changes' + this.common.generateQuery(options)
      });
    },
    getCredits: (options) => {
      return this.common.client({
        url: 'movie/' + options.id + '/credits' + this.common.generateQuery(options)
      });
    },
    getExternalIds: (options) => {
      return this.common.client({
        url: 'movie/' + options.id + '/external_ids' + this.common.generateQuery(options)
      });
    },
    getImages: (options) => {
      return this.common.client({
        url: 'movie/' + options.id + '/images' + this.common.generateQuery(options)
      });
    },
    getKeywords: (options) => {
      return this.common.client({
        url: 'movie/' + options.id + '/keywords' + this.common.generateQuery(options)
      });
    },
    getReleases: (options) => {
      return this.common.client({
        url: 'movie/' + options.id + '/release_dates' + this.common.generateQuery(options)
      });
    },
    getVideos: (options) => {
      return this.common.client({
        url: 'movie/' + options.id + '/videos' + this.common.generateQuery(options)
      });
    },
    getTranslations: (options) => {
      return this.common.client({
        url: 'movie/' + options.id + '/translations' + this.common.generateQuery(options)
      });
    },
    getRecommendations: (options) => {
      return this.common.client({
        url: 'movie/' + options.id + '/recommendations' + this.common.generateQuery(options)
      });
    },
    getSimilarMovies: (options) => {
      return this.common.client({
        url: 'movie/' + options.id + '/similar' + this.common.generateQuery(options)
      });
    },
    getReviews: (options) => {
      return this.common.client({
        url: 'movie/' + options.id + '/reviews' + this.common.generateQuery(options)
      });
    },
    getLists: (options) => {
      return this.common.client({
        url: 'movie/' + options.id + '/lists' + this.common.generateQuery(options)
      });
    },
    getLatest: () => {
      return this.common.client({
        url: 'movie/latest' + this.common.generateQuery()
      });
    },
    getUpcoming: (options) => {
      return this.common.client({
        url: 'movie/upcoming' + this.common.generateQuery(options)
      });
    },
    getNowPlaying: (options) => {
      return this.common.client({
        url: 'movie/now_playing' + this.common.generateQuery(options)
      });
    },
    getPopular: (options) => {
      return this.common.client({
        url: 'movie/popular' + this.common.generateQuery(options)
      });
    },
    getTopRated: (options) => {
      return this.common.client({
        url: 'movie/top_rated' + this.common.generateQuery(options)
      });
    },
    rate: (options, rate) => {
      return this.common.client({
          method: 'POST',
          status: 201,
          url: 'movie/' + options.id + '/rating' + this.common.generateQuery(options),
          body: {
            'value': rate
        }
      });
    },
    rateGuest: (options, rate) => {
      return this.common.client({
          method: 'POST',
          status: 201,
          url: 'movie/' + options.id + '/rating' + this.common.generateQuery(options),
          body: {
            'value': rate
        }
      });
    },
    removeRate: (options, rate) => {  
      return this.common.client({
          method: 'DELETE',
          status: 200,
        url: 'movie/' + options.id + '/rating' + this.common.generateQuery(options),
      });
    },
    removeRateGuest: (options, rate) => {
      return this.common.client({
          method: 'DELETE',
          status: 200,
        url: 'movie/' + options.id + '/rating' + this.common.generateQuery(options),
      });
    }
  };

  networks = {
    getById: (options) => {
      return this.common.client({
        url: 'network/' + options.id + this.common.generateQuery(options)
      });
    },
    getAlternativeNames: (options) => {
      return this.common.client({
        url: 'network/' + options.id + '/alternative_names' + this.common.generateQuery(options)
      });
    }
  };

  people = {
    getById: (options) => {
      return this.common.client({
        url: 'person/' + options.id + this.common.generateQuery(options)
      });
    },
    getMovieCredits: (options) => {
      return this.common.client({
        url: 'person/' + options.id + '/movie_credits' + this.common.generateQuery(options)
      });
    },
    getTvCredits: (options) => {
      return this.common.client({
        url: 'person/' + options.id + '/tv_credits' + this.common.generateQuery(options)
      });
    },
    getCredits: (options) => {
      return this.common.client({
        url: 'person/' + options.id + '/combined_credits' + this.common.generateQuery(options)
      });
    },
    getExternalIds: (options) => {
      return this.common.client({
        url: 'person/' + options.id + '/external_ids' + this.common.generateQuery(options)
      });
    },
    getImages: (options) => {
      return this.common.client({
        url: 'person/' + options.id + '/images' + this.common.generateQuery(options)
      });
    },
    getTaggedImages: (options) => {
      return this.common.client({
        url: 'person/' + options.id + '/tagged_images' + this.common.generateQuery(options)
      });
    },
    getChanges: (options) => {
      return this.common.client({
        url: 'person/' + options.id + '/changes' + this.common.generateQuery(options)
      });
    },
    getPopular: (options) => {
      return this.common.client({
        url: 'person/popular' + this.common.generateQuery(options)
      });
    },
    getLatest: () => {
      return this.common.client({
        url: 'person/latest' + this.common.generateQuery()
      });
    }
  };

  reviews = {
    getById: (options) => {
      return this.common.client({
        url: 'review/' + options.id + this.common.generateQuery(options)
      });
    }
  };

  search = {
    getMovie: (options) => {
      return this.common.client({
        url: 'search/movie' + this.common.generateQuery(options)
      });
    },
    getCollection: (options) => {
      return this.common.client({
        url: 'search/collection' + this.common.generateQuery(options)
      });
    },
    getTv: (options) => {
      return this.common.client({
        url: 'search/tv' + this.common.generateQuery(options)
      });
    },
    getPerson: (options) => {
      return this.common.client({
        url: 'search/person' + this.common.generateQuery(options)
      });
    },
    getCompany: (options) => {
      return this.common.client({
        url: 'search/company' + this.common.generateQuery(options)
      });
    },
    getKeyword: (options) => {
      return this.common.client({
        url: 'search/keyword' + this.common.generateQuery(options)
      });
    },
    getMulti: (options) => {
      return this.common.client({
        url: 'search/multi' + this.common.generateQuery(options)
      });
    }
  };

  tv = {
    getById: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + this.common.generateQuery(options)
      });
    },
    getAccountStates: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/account_states' + this.common.generateQuery(options)
      });
    },
    getAccountStatesGuest: (options) => {  
      return this.common.client({
        url: 'tv/' + options.id + '/account_states' + this.common.generateQuery(options)
      });
    },
    getAlternativeTitles: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/alternative_titles' + this.common.generateQuery(options)
      });
    },
    getChanges: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/changes' + this.common.generateQuery(options)
      });
    },
    getContentRatings: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/content_ratings' + this.common.generateQuery(options)
      });
    },
    getCredits: (options) => {  
      return this.common.client({
        url: 'tv/' + options.id + '/credits' + this.common.generateQuery(options)
      });
    },
    getExternalIds: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/external_ids' + this.common.generateQuery(options)
      });
    },
    getImages: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/images' + this.common.generateQuery(options)
      });
    },
    getKeywords: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/keywords' + this.common.generateQuery(options)
      });  
    },
    getRecommendations: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/recommendations' + this.common.generateQuery(options)
      });
    },
    getReviews: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/reviews' + this.common.generateQuery(options)
      });
    },
    getScreenedTheatrically: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/screened_theatrically' + this.common.generateQuery(options)
      });
    },
    getSimilar: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/similar' + this.common.generateQuery(options)
      });
    },
    getTranslations: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/translations' + this.common.generateQuery(options)
      });
    },
    getVideos: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/videos' + this.common.generateQuery(options)
      });
    },
    getAiringToday: (options) => {
      return this.common.client({
        url: 'tv/airing_today' + this.common.generateQuery(options)
      });
    },
    getLatest: (options) => {
      return this.common.client({
        url: 'tv/latest' + this.common.generateQuery(options)
      });
    },
    getOnTheAir: (options) => {
      return this.common.client({
        url: 'tv/on_the_air' + this.common.generateQuery(options)
      });
    },
    getPopular: (options) => {
      return this.common.client({
        url: 'tv/popular' + this.common.generateQuery(options)
      });
    },
    getTopRated: (options) => {
      return this.common.client({
        url: 'tv/top_rated' + this.common.generateQuery(options)
      });
    },
    rate: (options, rate) => {
      return this.common.client({
        method: 'POST',
        status: 201,
        url: 'tv/' + options.id + '/rating' + this.common.generateQuery(options),
        body: {
          'value': rate
        }
      });
    },
    rateGuest: (options, rate) => {
      return this.common.client({
        method: 'POST',
        status: 201,
        url: 'tv/' + options.id + '/rating' + this.common.generateQuery(options),
        body: {
          'value': rate
        }
      });
    },
    removeRate: (options, rate) => {
      return this.common.client({
        method: 'DELETE',
        status: 200,
        url: 'tv/' + options.id + '/rating' + this.common.generateQuery(options),
      });
    },
    removeRateGuest: (options, rate) => {
      return this.common.client({
        method: 'DELETE',
        status: 200,
        url: 'tv/' + options.id + '/rating' + this.common.generateQuery(options),
      });
    }
  };

  tvSeasons = {
    getById: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/season/' + options.season_number + this.common.generateQuery(options)
      });
    },
    getChanges: (options) => {  
      return this.common.client({
        url: 'tv/season/' + options.id + '/changes' + this.common.generateQuery(options)
      });
    },
    getAccountStates: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/season/' + options.season_number + '/account_states' + this.common.generateQuery(options)
      });
    },
    getAccountStatesGuest: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/season/' + options.season_number + '/account_states' + this.common.generateQuery(options)
      });
    },
    getCredits: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/season/' + options.season_number + '/credits' + this.common.generateQuery(options)
      });
    },
    getExternalIds: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/season/' + options.season_number + '/external_ids' + this.common.generateQuery(options)
      });
    },
    getImages: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/season/' + options.season_number + '/images' + this.common.generateQuery(options)
      });
    },
    getVideos: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/season/' + options.season_number + '/videos' + this.common.generateQuery(options)
      });
    }
  };

  tvEpisodes = {
    getById: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/season/'
          + options.season_number + '/episode/' + options.episode_number + this.common.generateQuery(options)
      });
    },
    getChanges: (options) => {
      return this.common.client({
        url: 'tv/episode/' + options.id + '/changes' + this.common.generateQuery(options)
      });
    },
    getAccountStates: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/season/'
          + options.season_number + '/episode/' + options.episode_number + '/account_states' + this.common.generateQuery(options)
      });
    },
    getAccountStatesGuest: (options) => {  
      return this.common.client({
        url: 'tv/' + options.id + '/season/'
          + options.season_number + '/episode/' + options.episode_number + '/account_states' + this.common.generateQuery(options)
      });
    },
    getCredits: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/season/'
          + options.season_number + '/episode/' + options.episode_number + '/credits' + this.common.generateQuery(options)
      });
    },
    getExternalIds: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/season/'
          + options.season_number + '/episode/' + options.episode_number + '/external_ids' + this.common.generateQuery(options)
      });
    },
    getImages: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/season/'
          + options.season_number + '/episode/' + options.episode_number + '/images' + this.common.generateQuery(options)
      });
    },
    getVideos: (options) => {
      return this.common.client({
        url: 'tv/' + options.id + '/season/'
          + options.season_number + '/episode/' + options.episode_number + '/videos' + this.common.generateQuery(options)
      });
    },
    rate: (options, rate) => {
      return this.common.client({
        method: 'POST',
        status: 201,
        url: 'tv/' + options.id + '/season/'
          + options.season_number + '/episode/' + options.episode_number + '/rating' + this.common.generateQuery(options),
        body: {
          'value': rate
        }
      });
    },
    rateGuest: (options, rate) => {
      return this.common.client({
        method: 'POST',
        status: 201,
        url: 'tv/' + options.id + '/season/'
          + options.season_number + '/episode/' + options.episode_number + '/rating' + this.common.generateQuery(options),
        body: {
          'value': rate
        }
      });
    },
    removeRate: (options, rate) => {
      return this.common.client({
        method: 'DELETE',
        status: 200,
        url: 'tv/' + options.id + '/season/'
          + options.season_number + '/episode/' + options.episode_number + '/rating' + this.common.generateQuery(options)
      });
    },
    removeRateGuest: (options, rate) => {
      return this.common.client({
        method: 'DELETE',
        status: 200,
        url: 'tv/' + options.id + '/season/'
          + options.season_number + '/episode/' + options.episode_number + '/rating' + this.common.generateQuery(options)
      });
    }
  };

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}

import { Component, OnDestroy } from '@angular/core';
import {TranslateService} from '@ngx-translate/core';
import { Subscription } from 'rxjs';

import { SettingsService } from './settings/settings.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnDestroy {
  title = 'app';
  private subscriptions: Subscription[] = [];
  private language: string;

  constructor(translate: TranslateService,
              private settingsService: SettingsService) {
    this.subscriptions.push(this.settingsService.language$.subscribe(lang => {
      this.language = lang;
      translate.use(this.language);
    }));

    translate.setDefaultLang(this.language);
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}

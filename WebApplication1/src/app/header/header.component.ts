import { Component, OnDestroy } from '@angular/core';
import { Subscription } from 'rxjs';
import { SettingsService } from '../settings/settings.service';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.less']
})
export class HeaderComponent implements OnDestroy {
  currentLang: string;
  availableLangs = [
    'en-US',
    'ru-RU'
  ];
  
  private subscriptions: Subscription[] = [];

  constructor(private settingsService: SettingsService) {
    this.subscriptions.push(this.settingsService.language$.subscribe(lang => this.currentLang = lang));
  }

  changeLanguage(lang: string) {
    this.settingsService.language = lang;
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}

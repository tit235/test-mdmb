import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HttpClient } from '@angular/common/http';
import { HttpModule } from '@angular/http';

import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppRoutingModule, routableComponents } from './app-routing.module';

import { AppComponent } from './app.component';
import { SettingsService } from './settings/settings.service';
import { TmdbService } from './api/tmdb.service';
import { GenreService } from './genre/genre.service';

import { MovieCardComponent } from './movie/movie-card/movie-card.component';
import { SidebarComponent } from './sidebar/sidebar.component';
import { HeaderComponent } from './header/header.component';
import { MovieActorCardComponent } from './movie/movie-details/movie-actor-card/movie-actor-card.component';

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http, './assets/i18n/', '.json');
}

@NgModule({
  declarations: [
    AppComponent,
    MovieCardComponent,
    SidebarComponent,
    HeaderComponent,
    MovieActorCardComponent,
    ...routableComponents
  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    HttpModule,
    TranslateModule.forRoot({
      loader: {
          provide: TranslateLoader,
          useFactory: HttpLoaderFactory,
          deps: [HttpClient]
      }
    }),
    NgbModule.forRoot(),
    AppRoutingModule
  ],
  providers: [
    SettingsService,
    TmdbService,
    GenreService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

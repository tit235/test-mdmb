import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs';

@Injectable()
export class SettingsService {
  language$: BehaviorSubject<string> = new BehaviorSubject<string>('en-US');

  set language(val: string) {
    if (!val) {
      return;
    }

    this.language$.next(val);
  }
}

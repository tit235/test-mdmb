import { Injectable, OnDestroy } from '@angular/core';
import { BehaviorSubject, Subscription } from 'rxjs';

import { TmdbService } from '../api/tmdb.service';
import { IGenre } from './genre.interface';
import { SettingsService } from '../settings/settings.service';

@Injectable()
export class GenreService implements OnDestroy {
  private subscriptions: Subscription[] = [];
  genres$: BehaviorSubject<IGenre[]> = new BehaviorSubject<IGenre[]>([]);

  constructor(private tmdbService: TmdbService,
              private settingsService: SettingsService) {
    this.subscriptions.push(this.settingsService.language$.subscribe(lang => {
      this.fetch();
    }));
  }

  private fetch() {
    this.tmdbService.genres.getMovieList()
    .then(res => {
      if (!res) {
        return;
      }
  
      this.genres$.next(res.genres);
    });
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}

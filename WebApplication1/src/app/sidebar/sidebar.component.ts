import { Component, OnDestroy } from '@angular/core';

import { GenreService } from '../genre/genre.service';
import { IGenre } from '../genre/genre.interface';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html'
})
export class SidebarComponent implements OnDestroy {
  genres: IGenre[] = [];
  private subscriptions: Subscription[] = [];

  constructor(private genreService: GenreService) {
    this.subscriptions.push(this.genreService.genres$.subscribe(res => this.genres = res));
  }

  ngOnDestroy() {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }
}

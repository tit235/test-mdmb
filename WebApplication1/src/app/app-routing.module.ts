import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { MovieListComponent } from './movie/movie-list/movie-list.component';
import { MovieListPageType } from './movie/movie-list/movie-list-page-type';
import { MovieDetailsComponent } from './movie/movie-details/movie-details.component';
 
const appRoutes: Routes = [
  {
    path: 'movie',
    children: [
      {
        path: 'now-playing',
        component: MovieListComponent,
        data: {
          page: MovieListPageType.NowPlaying
        }
      },
      {
        path: 'discover',
        component: MovieListComponent,
        data: {
          page: MovieListPageType.Discover
        }
      },
      {
        path: ':id',
        component: MovieDetailsComponent
      }
    ]
  }
];
 
@NgModule({
  imports: [
    RouterModule.forRoot(appRoutes)
  ],
  exports: [
    RouterModule
  ]
})
export class AppRoutingModule {}

export const routableComponents = [
  MovieListComponent,
  MovieDetailsComponent
];
